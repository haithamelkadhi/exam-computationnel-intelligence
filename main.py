import numpy as np
import pandas as pd
from sklearn import neighbors, metrics, svm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import LabelEncoder, LabelBinarizer
from matplotlib import pyplot as plt

data = pd.read_csv('DataSet.data')
print(data.head())
X = data[['Candidacy_documents', 'Score_High_School', 'Frensh_level','Admission_test_result']].values
y = data[['Admission decision']].values
X = np.array(X)
print(X,y)

Le = LabelEncoder()
for i in range(len(X[0])):
    X[:, i] = Le.fit_transform(X[:, i])
print(X)

Lee = LabelBinarizer()
for i in range(len(y[0])):
    y= Lee.fit_transform(y)
print(y)
print(X.shape)
print(y.shape)
knn = neighbors.KNeighborsClassifier(n_neighbors=25,weights='uniform')
X_train, X_test,y_train, y_test = train_test_split(X, y, test_size=0.2)

knn.fit(X_train,y_train)
prediction = knn.predict(X_test)
accuracy = metrics.accuracy_score(y_test, prediction)
print("predictions:", prediction)
print("accuracy: ", accuracy)
a = 500
print("actual value ", y[a])
print("predicted value", knn.predict(X)[a])

